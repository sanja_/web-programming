﻿document.forma.reset();

function scale_of_notation()
  {
  var flag=true;
  var regex=[/\D/,/\D|[2-9]/,/\D|[8-9]/,/[G-zА-я]/,/\S/];
  var notation=[10,2,8,16];
  var number=parseInt(document.forma.input_number.value,document.getElementById("select_notation").value);

  validate_form();

  for(var i=0;i<4;i++)
    { 
    if(document.getElementById("select_notation").value==notation[i] && regex[i].test(document.forma.input_number.value)==false)
      { 
      document.forma.notation_2.value=(number.toString(2));
      document.forma.notation_8.value=(number.toString(8));
      document.forma.notation_10.value=(number.toString(10));
      document.forma.notation_16.value=(number.toString(16));
      flag=false;
      exit;
      }       
    }

  if(flag)
    {
    alert("Неправильно введены данные.Пожалуйста повторите ввод!");
    }

  function validate_form()
    {
    if(regex[4].test(document.forma.input_number.value)==false)
      {
      alert ( "Поле для ввода числа пустует ,либо вы ввели не верные данные.Пожалуйста повторите  ввод! ." );
      exit;
      }
    }  
  }